﻿using UnityEngine;
using System.Collections;

public class SoundScale : MonoBehaviour {

    private int m_qSample = 2048;
    private float m_rmsValue = 0;

    private AudioSource m_audioSource;

    private float[] m_samples;

    [SerializeField]
    private int m_channel = 0;

	// Use this for initialization
	void Start () {
        m_audioSource = GetComponent<AudioSource>();
        m_samples = new float[m_qSample];
	}
	
	// Update is called once per frame
	void Update () {
        AnalyseSound();

        Vector3 scale = transform.localScale;
        scale.y = 0.5f + (m_rmsValue * 5);
        transform.localScale = scale;

        Vector3 pos = transform.position;
        pos.y = scale.y * 0.5f;
        transform.position = pos; 
	}

    private void AnalyseSound()
    {
        m_audioSource.GetOutputData(m_samples, m_channel);

        float sum = 0;

        for (int i = 0; i < m_samples.Length; i++)
        {
            sum += m_samples[i] * m_samples[i];
        }

        m_rmsValue = Mathf.Sqrt(sum / m_qSample);
    }
}
    
