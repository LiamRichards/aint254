﻿using UnityEngine;
using System.Collections;
using UnityEngine.Audio;

public class SoundManager : MonoBehaviour {

    [SerializeField]
    private AudioMixer m_mixer;
    [SerializeField]
    private AudioMixerSnapshot m_gameMode;
    [SerializeField]
    private AudioMixerSnapshot m_menuMode;

	public void setVolume(float _volume)
    {
        m_mixer.SetFloat("MasterVol", (_volume * 100) - 80);
    }

    public void MenuModeOn()
    {
        m_menuMode.TransitionTo(0.5f);
    }

    public void GameModeOn()
    {
        m_gameMode.TransitionTo(0.5f);
    }
}
