﻿using UnityEngine;
using System.Collections;

public class MemoryAlloc : MonoBehaviour {

    private Transform m_transform;
    private GameObject[] m_gameObjArray;

    private string m_string1;
    private string m_string2;

    private int m_hashString1;
    private int m_hashString2;

	// Use this for initialization
	void Start () {
        m_hashString1 = m_string1.GetHashCode();
        m_hashString2 = m_string2.GetHashCode();

        m_transform = transform;
        m_gameObjArray = new GameObject[1000];

        for (int i = 0; i < 1000; i++)
        {
            GameObject obj = Instantiate(gameObject);
            m_gameObjArray[i] = obj;
            m_gameObjArray[i].SetActive(false);
        }
	}
	
	// Update is called once per frame
	void Update () {
        //TransformTest();
        //TransformTestOptimised();
        //ObjectAlloc();
        //InstantiateTest();
        ArrayTest();
    }

    private void TransformTest()
    {
        for (int i = 0; i < 10000; i++)
        {
            transform.position = Vector3.zero;
        }
    }

    private void TransformTestOptimised()
    {
        for (int i = 0; i < 10000; i++)
        {
            m_transform.position = Vector3.zero;
        }
    }

    private void ObjectAlloc()
    {
        int testInt = 1;
        int testInt1 = testInt;

        for (int i = 0; i < 10000; i++)
        {
            TestObject objectTemp = new TestObject();
        }
    }

    private void InstantiateTest()
    {
        for (int i = 0; i < 1000; i++)
        {
            GameObject obj = Instantiate(gameObject);
            obj.transform.position = new Vector3(1.0f, 0.0f, 0.0f);
            Destroy(obj);
        }
    }

    private void ArrayTest()
    {
        for (int i = 0; i < 1000; i++)
        {
            m_gameObjArray[i].SetActive(true);
            m_gameObjArray[i].transform.position = new Vector3(1.0f, 0.0f, 0.0f);
            m_gameObjArray[i].SetActive(false);
        }
    }

    private float[] RandomValues(int _numbers)
    {
        var result = new float[_numbers];

        for (int i = 0; i < result.Length; i++)
        {
            result[i] = Random.value;

        }

        return result;
    }

    private void RandomValuesOptimised(float[] _arrayToFill)
    {
        for (int i = 0; i < _arrayToFill.Length; i++)
        {
            _arrayToFill[i] = Random.value;
        }
    }

    private bool CompareTwoStrings(string _string1, string _string2)
    {
        if (_string1 == _string2)
            return true;

        return false;
    }

    private bool CompareTwoInt(int _int1, int _int2)
    {
        if (_int1 == _int2)
            return true;

        return false;
    }

    private bool CompareTwoInt(string _int1, string _int2)
    {
        if (_int1.GetHashCode() == _int2.GetHashCode())
            return true;

        return false;
    }
}
