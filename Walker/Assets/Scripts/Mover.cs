﻿using UnityEngine;
using System.Collections;

namespace Mover
{
    public class Mover : MonoBehaviour {

        private Transform m_transform;
        private int[] m_intArray;
        int randomNum;
        public AnimationCurve curve;

	    // Use this for initialization
	    void Start () {

            m_transform = GetComponent<Transform>();
            m_intArray = new int[5];

            m_intArray[0] = 0;
            m_intArray[1] = 0;
            m_intArray[2] = 1;
            m_intArray[3] = 2;
            m_intArray[4] = 3;

        }
	
	    // Update is called once per frame
	    void Update () {

            //Step();
            //ArrayStep();
            //FloatProb();
            //Curve();
            //Vector();
            Perlin();
	    }

        private void Step()
        {
            randomNum = Random.Range(0, 4);

            float xValue = 0.0f;
            float zValue = 0.0f;

            if (randomNum == 0)
            {
                xValue = 0.3f;
            }
            else if (randomNum == 1)
            {
                xValue = -0.3f;
            }
            else if (randomNum == 2)
            {
                zValue = 0.3f;
            }
            else
            {
                zValue = -0.3f;
            }

            m_transform.position += new Vector3(xValue, 0.0f, zValue);
        }

        private void ArrayStep()
        {
            int randomArray = Random.Range(0, 5);

            randomNum = m_intArray[randomArray];

            float xValue = 0.0f;
            float zValue = 0.0f;

            if (randomNum == 0)
            {
                xValue = 0.3f;
            }
            else if (randomNum == 1)
            {
                xValue = -0.3f;
            }
            else if (randomNum == 2)
            {
                zValue = 0.3f;
            }
            else
            {
                zValue = -0.3f;
            }

            m_transform.position += new Vector3(xValue, 0.0f, zValue);

        }

        private void FloatProb()
        {
            float randomNum = Random.Range(0.0f, 1.0f);

            float xValue = 0.0f;
            float zValue = 0.0f;

            if (randomNum < 0.4f)
            {
                xValue = 0.3f;
            }
            else if (randomNum < 0.8f)
            {
                xValue = -0.3f;
            }
            else if (randomNum < 0.9f)
            {
                zValue = 0.3f;
            }
            else
            {
                zValue = -0.3f;
            }

            m_transform.position += new Vector3(xValue, 0.0f, zValue);

        }

        private void Curve()
        {
            float randomCurve = Random.Range(0.0f, 1.0f);
            float randomNum = curve.Evaluate(randomCurve);

            float xValue = 0.0f;
            float zValue = 0.0f;

            if (randomNum < 0.25f)
            {
                xValue = 0.3f;
            }
            else if (randomNum < 0.5f)
            {
                xValue = -0.3f;
            }
            else if (randomNum < 0.75f)
            {
                zValue = 0.3f;
            }
            else
            {
                zValue = -0.3f;
            }

            m_transform.position += new Vector3(xValue, 0.0f, zValue);
        }

        private void Vector()
        {
            float xValue = Random.Range(-0.3f, 0.3f);
            float zValue = Random.Range(-0.3f, 0.3f);

            m_transform.position += new Vector3(xValue, 0.0f, zValue);


        }

        private void Perlin()
        {
            //float x = Random.Range(-0.3f, 0.3f);
            //float z = Random.Range(-0.3f, 0.3f);

            float xScale = Random.Range(-0.1f, 0.1f);
            float zScale = Random.Range(-0.1f, 0.1f);

            float xValue = (xScale * Mathf.PerlinNoise(m_transform.position.x , 0.0f));
            float zValue = (zScale * Mathf.PerlinNoise(m_transform.position.z , 0.0f));

            m_transform.position += new Vector3(xValue, 0.0f, zValue);

            Debug.Log(zValue);
        }
    }
}