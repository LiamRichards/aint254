﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour {

    [SerializeField]
    private GameObject m_dot;

    private GameObject[] m_line;

    [SerializeField]
    private float m_force = 5.0f;

    private Vector3 m_direction;

    //[SerializeField]
    //private float m_yforce = 5.0f;


	// Use this for initialization
	void Start () {

        CreateArray();
        //CreateLine();
	}
	
	// Update is called once per frame
	void Update () {

        Debug.Log("Mouse x: " + Input.mousePosition.x + " Mouse y: " + Input.mousePosition.y);

        Vector3 screenPos = Camera.main.WorldToScreenPoint(transform.position);
        Vector3 direction = (Input.mousePosition - screenPos).normalized;

        if (Input.GetMouseButton(0))
        {
            for (int i = 0; i < m_line.Length; i++)
            {
                m_line[i].SetActive(true);
            }
            screenPos.z = 0;
            Aim(new Vector3(direction.x, direction.y, 0));
        }
        else
        {
            for (int i = 0; i < m_line.Length; i++)
            {
                m_line[i].SetActive(false);
            }
        }

        if(Input.GetMouseButtonUp(0))
        {
            transform.GetComponent<Rigidbody>().velocity = direction * m_force;
        }     
        
           
    }

    // Creates an array of dots
    void CreateArray ()
    {
        m_line = new GameObject[10];

        for (int i = 0; i < m_line.Length; i++)
        {
            m_line[i] = Instantiate(m_dot, Vector3.zero, Quaternion.identity) as GameObject;
        }
    }

    private void Aim(Vector3 direction)
    {
        float Vox = direction.x * m_force;
        float Voy = direction.y * m_force;
        float t = 0;

        for (int i = 0; i < m_line.Length; i++)
        {
            t = i * 0.1f;

            m_line[i].transform.position = new Vector3(transform.position.x + (Vox * t), transform.position.y + (Voy * t - 0.5f * 9.81f * (t* t)), 0.0f);
        }
    }
}
