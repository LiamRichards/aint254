﻿using UnityEngine;
using System.Collections;

namespace ISS
{

    public class Follow : MonoBehaviour
    {

        private Transform m_transform;
        private float mouseX;
        private float mouseY;

        // Use this for initialization
        void Start()
        {
            m_transform = transform;
        }

        // Update is called once per frame
        void Update()
        {
            Vector3 point = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 10));
            Vector3 direction = point - m_transform.position;

            float angle = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg;
            Debug.Log(mouseX);

            //m_transform.eulerAngles = new Vector3(10.0f, angle, 0.0f);
            //m_transform.Rotate(0, angle, 0);
            //m_transform.rotation = new Quaternion(0.0f, angle, 0.0f, 0.0f);
            m_transform.rotation = Quaternion.Euler(new Vector3(0.0f, angle, 0.0f));

            m_transform.position += direction * 0.1f;
        }

        void OnDrawGizmosSelected()
        {
            Vector3 point = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 10));

            Gizmos.color = Color.green;
            Gizmos.DrawSphere(point, 0.1f);
        }
    }
}