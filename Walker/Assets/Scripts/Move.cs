﻿using UnityEngine;
using System.Collections;

public class Move : MonoBehaviour {

    [SerializeField]
    private float m_speed = 5.0f;

    [SerializeField]
    private float m_rotationSpeed = 50.0f;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        float rotY = transform.rotation.eulerAngles.y;

        float extraRot = m_rotationSpeed * Input.GetAxis("Horizontal") * Time.deltaTime;

        transform.rotation = Quaternion.Euler(0.0f, rotY + extraRot, 0.0f);
        
	}
}
