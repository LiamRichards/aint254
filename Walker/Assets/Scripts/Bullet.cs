﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour {

    private Rigidbody m_rigidbody;
    [SerializeField]
    private float m_force = 200;
    private Transform m_transform;

	// Use this for initialization
	void Start () {
        m_transform = transform;
        m_rigidbody = m_transform.GetComponent<Rigidbody>();
        m_rigidbody.AddForce(m_transform.forward * m_force);
	}
	
	// Update is called once per frame
	void Update () {
	    
	}
}
