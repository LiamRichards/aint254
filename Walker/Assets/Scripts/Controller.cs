﻿using UnityEngine;
using System.Collections;

public class Controller : MonoBehaviour {

	void OnGUI()
    {
        GUI.Label(new Rect(5, 5, 200, 25), "LeftJoyStick: ");
        GUI.Label(new Rect(135, 5, 200, 25), Input.GetAxis("Horizontal").ToString());
        GUI.Label(new Rect(205, 5, 200, 25), Input.GetAxis("Vertical").ToString());

        GUI.Label(new Rect(5, 20, 200, 25), "RightJoyStick: ");
        GUI.Label(new Rect(135, 20, 200, 25), Input.GetAxis("RightX").ToString());
        GUI.Label(new Rect(205, 20, 200, 25), Input.GetAxis("LeftX").ToString());

        GUI.Label(new Rect(5, 35, 200, 25), "A: ");
        GUI.Label(new Rect(135, 35, 200, 25), KeyCode.JoystickButton0.ToString());
    }
}
