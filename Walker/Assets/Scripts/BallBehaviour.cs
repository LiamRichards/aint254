﻿using UnityEngine;
using System.Collections;

namespace ISS
{
    public class BallBehaviour : MonoBehaviour {

        private Entity m_entity;
	    // Use this for initialization
	    void Start () {
            m_entity = new Entity();
	    }
	
	    // Update is called once per frame
	    void Update () {

            if(Input.GetKey(KeyCode.W))
            {
                m_entity.UpdateAccel(new Vector3(0.0f, 0.0f, 1.0f) * 0.001f);
            }
            else if (Input.GetKey(KeyCode.A))
            {
                m_entity.UpdateAccel(new Vector3(-1.0f, 0.0f, 1.0f) * 0.001f);
            }
            else if (Input.GetKey(KeyCode.S))
            {
                m_entity.UpdateAccel(new Vector3(0.0f, 0.0f, -1.0f) * 0.001f);
            }
            else if (Input.GetKey(KeyCode.D))
            {
                m_entity.UpdateAccel(new Vector3(1.0f, 0.0f, 0.0f) * 0.001f);
            }
            else
            {
                m_entity.UpdateAccel(new Vector3(0.0f, 0.0f, 0.0f) * 0.00f);
            }

            //m_entity.UpdateAccel(new Vector3(0.0f, 0.0f, 0.0f) * 0.001f);
            m_entity.update();
            transform.position = m_entity.GetPosition();
	    }
    }

}

