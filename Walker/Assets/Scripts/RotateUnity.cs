﻿using UnityEngine;
using System.Collections;

namespace ISS
{
    public class RotateUnity : MonoBehaviour
    {
        [SerializeField]
        private float m_rotationSpeed = 5.0f;

        private Transform m_transform;

        // Use this for initialization
        void Start()
        {
            m_transform = transform;
        }

        // Update is called once per frame
        void Update()
        {
            m_transform.Rotate(new Vector3(1, 0, 0) * m_rotationSpeed * Time.deltaTime, Space.World);
            m_transform.RotateAround(new Vector3(3.0f, 3.0f, 0.0f), new Vector3(0.0f, 1.0f, 0.0f), m_rotationSpeed * Time.deltaTime);
        }
    }
}
