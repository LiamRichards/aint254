﻿using UnityEngine;
using System.Collections;

public class MoveCap : MonoBehaviour {

    private Rigidbody m_rigidbody;
    [SerializeField]
    private float m_speed = 10.0f;

	// Use this for initialization
	void Start () {
        m_rigidbody = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {
	    if (Input.GetAxis("Vertical") > 0)
        {
            m_rigidbody.AddForce(transform.forward * m_speed * Time.deltaTime);
        }
	}
}
