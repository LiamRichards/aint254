﻿using UnityEngine;
using System.Collections;


namespace ISS
{
   public class Entity
    {

        private Vector3 m_position;
        private Vector3 m_velocity;
        private Vector3 m_accel;

        public Entity()
        {
            m_position = new Vector3(0.0f, 0.0f, 0.0f);
            m_velocity = new Vector3(0.0f, 0.0f, 0.0f);
            m_accel = new Vector3(0.0f, 0.0f, 0.0f);
        }

        private void ChangePos()
        {
            m_position += m_velocity;
        }

        public Vector3 GetPosition()
        {
            return m_position;
        }

        private void UpdateVelocity()
        {
            m_velocity += m_accel;
        }

        public void UpdateAccel(Vector3 accel)
        {
            m_accel = accel;
            
        }

        public void update()
        {
            UpdateVelocity();
            ChangePos();
        }
    }
}