﻿using UnityEngine;
using System.Collections;

public class Snake : MonoBehaviour {

    [SerializeField]
    private GameObject m_segment;

    private Transform[] m_snake;

    [SerializeField]
    private float m_amplitude = 0.6f;

    private float angle = 0.0f;
    private float angleVelocity = 100f;

    [SerializeField]
    [Range(1000.001f, 1100f)]
    private float m_period = 1010f;

	// Use this for initialization
	void Start () {
        CreateSnake();
	}
	
	// Update is called once per frame
	void Update () {

        transform.position = new Vector3(transform.position.x, 0.0f, m_snake[1].position.z);

        for(int i = 0; i < m_snake.Length; i++)
        {
            float xValue = m_amplitude * Mathf.Sin(2 * Mathf.PI * angle / m_period);
            m_snake[i].position = new Vector3(xValue, 0.0f, m_snake[i].position.z + 0.1f);
            angle += angleVelocity;
        }

	}

    private Transform CreateSegment()
    {
        GameObject tempObject = Instantiate(m_segment);

        Transform tempTrans = tempObject.GetComponent<Transform>();

        return tempTrans;
    }

    private void CreateSnake()
    {
        m_snake = new Transform[100];

        for(int i = 0; i < m_snake.Length; i++)
        {
            m_snake[i] = CreateSegment();

            m_snake[i].position = new Vector3(0, 0, i * 0.25f);
        }
    }
}
