﻿using UnityEngine;
using System.Collections;

public class RightBike : MonoBehaviour {

    private Rigidbody m_Rigidbody;

    [SerializeField]
    private GameObject m_FWheel;
    [SerializeField]
    private GameObject m_BWheel;
    private WheelCollider m_FWheelCol;
    private WheelCollider m_BWheelCol;
    private WheelHit m_FHit;
    private WheelHit m_BHit;

	// Use this for initialization
	void Start () {

        m_Rigidbody = GetComponent<Rigidbody>();
        m_FWheelCol = m_FWheel.GetComponent<WheelCollider>();
        m_BWheelCol = m_BWheel.GetComponent<WheelCollider>();

	}
	
	// Update is called once per frame
	void Update () {

        //transform.position += Vector3.up;
        if (m_Rigidbody.transform.rotation.z != 0)
        {
            m_Rigidbody.transform.rotation = Quaternion.Euler(new Vector3(transform.rotation.x, transform.rotation.y, 0.0f));

        }

        
        

        if (m_FWheelCol.isGrounded && m_BWheelCol.isGrounded)
        {
           
        }

	}
}
