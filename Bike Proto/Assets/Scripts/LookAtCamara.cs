﻿using UnityEngine;
using System.Collections;

public class LookAtCamara : MonoBehaviour {

    [SerializeField]
    private GameObject m_target;

    [SerializeField]
    private Vector3 offset;

    [SerializeField]
    private float damping = 1;

	// Use this for initialization
	void Start () {
        offset = transform.position - m_target.transform.position;
	}
	
	// Update is called once per frame
	void LateUpdate () {

        //Vector3 desiredPosition = m_target.transform.position + offset;
        //Vector3 position = Vector3.Lerp(transform.position, desiredPosition, Time.deltaTime * damping);
        //transform.position = desiredPosition;
        //transform.LookAt(m_target.transform);

        float desiredAngle = m_target.transform.eulerAngles.y;
        Quaternion rotation = Quaternion.Euler(0, desiredAngle, 0);
        transform.position = m_target.transform.position - (rotation * offset);
        transform.LookAt(m_target.transform);
    }
}
