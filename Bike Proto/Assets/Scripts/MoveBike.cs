﻿using UnityEngine;
using System.Collections;

public class MoveBike : MonoBehaviour {

    [SerializeField]
    private float m_speed = 5.0f;
    [SerializeField]
    private float m_rotationSpeed = 5.0f;
    [SerializeField]
    private GameObject m_COM;

    private Rigidbody m_rigidbody;
    private Transform m_transform;
    private float m_force;

    //[SerializeField]
    //private GameObject m_FWheel;
    //[SerializeField]
    //private GameObject m_BWheel;
    //private WheelCollider m_FWheelCol;
    //private WheelCollider m_BWheelCol;
    //private WheelHit m_FHit;
    //private WheelHit m_BHit;


    // Use this for initialization
    void Start () {
        m_rigidbody = GetComponent<Rigidbody>();
        m_transform = transform;
        m_rigidbody.centerOfMass = m_COM.transform.position;
        //m_FWheelCol = m_FWheel.GetComponent<WheelCollider>();
        //m_BWheelCol = m_BWheel.GetComponent<WheelCollider>();
    }

    // Update is called once per frame
    void Update () {
        if (Input.GetAxis("Vertical") > 0)
        {
            m_force = m_speed;
            //m_BWheelCol.brakeTorque = 0;
            //m_BWheelCol.motorTorque += m_speed;
            //m_FWheelCol.motorTorque += m_speed;
        }
        else if (Input.GetAxis("Vertical") < 0)
        {
            m_force = m_speed * -1;
            //m_BWheelCol.brakeTorque = 0;
            //m_BWheelCol.motorTorque += m_speed * -1;
            //m_FWheelCol.motorTorque += m_speed * -1 ;
        }
        else
        {
            //m_BWheelCol.motorTorque = 0;
            //m_BWheelCol.brakeTorque = 100;
        }

        
        if (Input.GetAxis("Horizontal") > 0)
        {
            //m_FWheelCol.steerAngle = 30;
            m_transform.Rotate(new Vector3(0, 1, 0) * m_rotationSpeed * Time.deltaTime, Space.World);
        }
        else if (Input.GetAxis("Horizontal") < 0)
        {
            //m_FWheelCol.steerAngle = -30;
            m_transform.Rotate(new Vector3(0, -1, 0) * m_rotationSpeed * Time.deltaTime, Space.World);
        }
        else
        {
            //m_FWheelCol.steerAngle = 0;
        }

        if (Input.GetKey(KeyCode.R))
        {
            m_rigidbody.transform.rotation = Quaternion.Euler(new Vector3(transform.rotation.x, transform.rotation.y, 0.0f));
        }

        if (Input.GetAxis("Fire1") > 0)
        {
            m_rigidbody.transform.rotation = Quaternion.Euler(new Vector3(transform.rotation.x, transform.rotation.y, 0.0f));
        }

        m_rigidbody.AddForce(transform.forward * m_force * Time.deltaTime);
        m_force = 0.0f;

        //m_BWheelCol.attachedRigidbody.AddForce(-transform.up * 100f * m_BWheelCol.attachedRigidbody.velocity.magnitude);


    }
}
