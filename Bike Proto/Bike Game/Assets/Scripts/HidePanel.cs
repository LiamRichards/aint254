﻿using UnityEngine;
using System.Collections;

public class HidePanel : MonoBehaviour {

    [SerializeField]
    private float disapearTime = 1.0f;
	// Use this for initialization

	void Start () {
        gameObject.SetActive(true);
        StartCoroutine(Hide());
        
	}
	
	// Update is called once per frame
	IEnumerator Hide () {

        yield return new WaitForSeconds(disapearTime);
        gameObject.SetActive(false);

    }
}
