﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Timer : MonoBehaviour {

    public float m_time;
    private bool isCounting = false;

    void Awake()
    {
        m_time = 0.0f;
    }

    void Update()
    {
        if(isCounting)
        {
            m_time += 1 * Time.deltaTime;
            GetComponent<Text>().text = "Time: " + (System.Math.Round(m_time,2)).ToString();
        }
    }

    public void StartTimer()
    {
        isCounting = true;
    }

    public void StopTimer()
    {
        isCounting = false;
    }

    public void ResetTimer()
    {
        m_time = 0.0f;
    }
}
