﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class PointSystem : MonoBehaviour {

    private List<GameObject> m_pointList = new List<GameObject>();

    public Component[] points;

    public int pointsLeft;

    [SerializeField]
    private GameObject Bike;

    [SerializeField]
    private GameObject Panel;

    [SerializeField]
    private GameObject m_timer;

	// Use this for initialization
	void Start () {

        CheckNumHit();

	}
	
	// Update is called once per frame
	void Update () {

        
    }

    public void CheckNumHit ()
    {
        points = GetComponentsInChildren<PointBehaviour>();

        //m_pointList.Clear();
        pointsLeft = 0;

        foreach (PointBehaviour point in points)
        {
            //m_pointList.Add(point.gameObject);
            if(!point.m_hit)
            {
                pointsLeft++;
            }
        }

        //pointsLeft = m_pointList.Count;

        //Canvas ui = GetComponentInParent<Canvas>();
        //Text pointText;
        //pointText = ui.GetComponentInChildren<Text>();
        //pointText.text = "Points Left: " + m_pointList.Count;

        Debug.Log(pointsLeft);

        if (pointsLeft == 0)
        {
            LevelComplete();
        }
    }

    void LevelComplete()
    {
        Bike.GetComponent<MoveBike>().enabled = false;
        Panel.SetActive(true);
        m_timer.GetComponent<Timer>().StopTimer();
        //Application.LoadLevel("Main");

        Debug.Log("Nep");
    }

    public void NextLevel()
    {
        Application.LoadLevel("Main");
    }
}
