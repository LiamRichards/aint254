﻿using UnityEngine;
using System.Collections;

public class AnimatePanel : MonoBehaviour {

    private Animator m_anima;
    private bool tog = false;

	// Use this for initialization
	void Start () {
        m_anima = GetComponent<Animator>();
	}
	
	public void Toggle()
    {
        if(!tog)
        {
            tog = true;
            m_anima.SetBool("isSelected", true);
        }
        else
        {
            tog = false;
            m_anima.SetBool("isSelected", false);
        }
    }
}
