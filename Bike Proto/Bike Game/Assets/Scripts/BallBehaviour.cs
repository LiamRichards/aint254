﻿using UnityEngine;
using System.Collections;

public class BallBehaviour : MonoBehaviour {

    private Transform m_transform;
    private Rigidbody m_rigidbody;

	// Use this for initialization
	void Start () {
        m_transform = transform;
        m_rigidbody = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {
        if (m_transform.position.y < -100)
        {
            m_transform.position = new Vector3(0, 10, 0);
            m_rigidbody.angularVelocity = Vector3.zero;
            m_rigidbody.velocity = Vector3.zero;
        }
    }
}
