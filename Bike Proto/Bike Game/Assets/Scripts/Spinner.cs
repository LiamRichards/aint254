﻿using UnityEngine;
using System.Collections;

public class Spinner : MonoBehaviour {

    [SerializeField]
    private float rotationSpeed = 1.0f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        transform.Rotate(new Vector3(0, 1, 0) * rotationSpeed);
	}
}
