﻿using UnityEngine;
using System.Collections;

public class GameSystem : MonoBehaviour 
{

	// Use this for initialization
	void Awake () 
    {
        DontDestroyOnLoad(gameObject);
	}

    public void LoadLevel(int _level)
    {
        Application.LoadLevel(_level);
    }
}
