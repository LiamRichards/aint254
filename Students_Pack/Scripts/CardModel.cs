﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class CardModel 
{
    public Sprite cardBack;     //the back of the card
    public Sprite cardSprite;   //front of the card
    public int id;              //card id - this is what we check, 
}
